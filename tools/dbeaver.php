<?php

$local = $argv[1] ?? false;
if (!shell_exec('platform tunnel:info --encode') && !$local) {
    echo 'tunnel not found.';
    exit(1);
}

if ($local) {
    $landoInfoJson = shell_exec('lando info --format=json');
    $landoInfo = json_decode($landoInfoJson, true);
    $db = $landoInfo[1];
    $name = 'local';
    $ip = $db['external_connection']['host'];
    $port = $db['external_connection']['port'];
    $path = $db['creds'][0]['path'];
    $username = $db['creds'][0]['user'];
    $password = $db['creds'][0]['password'];
} else {
    $relationships_encoded = shell_exec('platform tunnel:info --encode');
    $relationships = json_decode(base64_decode($relationships_encoded, TRUE), TRUE);
    $db = $relationships['database'][0];
    $name = 'serveur';
    $ip = $db['ip'];
    $port = $db['port'];
    $path = $db['path'];
    $username = $db['username'];
    $password = $db['password'];
}

$dbeaverCommand = "dbeaver -con \"name={$name}|driver=mysql|host={$ip}|port={$port}|database={$path}|user={$username}|password={$password}\"";
echo 'start dbeaver with command ' . $dbeaverCommand;
shell_exec($dbeaverCommand);
exit(0);