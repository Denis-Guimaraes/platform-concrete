<?php

$databaseFile = sprintf('%s/application/config/database.php', getenv('PLATFORM_DOCUMENT_ROOT'));
if (file_exists($databaseFile)) {
    exit(0);
}

$relationships = json_decode((base64_decode(getenv('PLATFORM_RELATIONSHIPS'))), true);
$db = $relationships['database'][0];

$args = [
    'db-server' => $db["host"],
    'db-username' => $db["username"],
    'db-password' => $db["password"],
    'db-database' => $db["path"],
    'site' => 'platform-concrete',
    'admin-email' => 'contact@6tematik.fr',
    'admin-password' => 'testPassword',
];

$argToString = '';
foreach ($args as $arg => $value) {
    $argToString .= sprintf(' --%s %s', $arg, $value);
}

$installCommand = sprintf('php ./vendor/bin/concrete5 c5:install%s --ignore-warnings --no-interaction', $argToString);
$outpout = null;
$resultCode = null;
try {
    var_dump('start install');
    exec($installCommand, $outpout, $resultCode);
    var_dump('finish install');
    var_dump($resultCode);
    exit(0);
} catch (\Throwable $th) {
    var_dump($th->getMessage());
    exit(1);
}
