<?php

$config = new \Platformsh\ConfigReader\Config();

if ($config->environment === 'lando') {
  $_SERVER['REMOTE_ADDR'] = $_SERVER['LANDO_HOST_IP'];
}